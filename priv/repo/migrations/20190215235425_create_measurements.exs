defmodule Moments.Repo.Migrations.CreateMeasurements do
  use Ecto.Migration

  def change do
    create table(:measurements) do
      add(:user_id, references("users"))
      add(:context, :string)
      add(:type, :integer)
      add(:field, :string)
      add(:timestamp, :utc_datetime)
      add(:value, :integer)
      add(:description, :text)

      timestamps(type: :utc_datetime)
    end

    create(index("measurements", [:timestamp]))
    create(index("measurements", [:user_id]))
    create(index("measurements", [:context]))
    create(index("measurements", [:field]))
    create(index("measurements", [:user_id, :context, :field, :timestamp]))
  end
end
