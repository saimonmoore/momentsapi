# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :moments,
  ecto_repos: [Moments.Repo]

# Configures the endpoint
config :moments, MomentsWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "TwlKLWNSTLdoe1oovify27TZEYFgxpRadZzuJpxDlddHCc1T0yeLPoefkuMSa8W6",
  render_errors: [view: MomentsWeb.ErrorView, accepts: ~w(json)],
  pubsub: [name: Moments.PubSub, adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

config :moments, Moments.Guardian, issuer: "moments", secret_key: "secret"

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
