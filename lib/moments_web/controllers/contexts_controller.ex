defmodule MomentsWeb.ContextsController do
  use MomentsWeb, :controller

  alias Moments.Statistics

  action_fallback(MomentsWeb.FallbackController)

  def index(conn, _params) do
    user = Guardian.Plug.current_resource(conn)

    contexts = Statistics.list_contexts(user)
    IO.puts("=====> contexts: #{inspect(contexts)}")

    json(conn, %{contexts: contexts})
  end
end
