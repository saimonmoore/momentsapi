defmodule MomentsWeb.MeasurementsController do
  use MomentsWeb, :controller

  alias Moments.Statistics
  alias Moments.Statistics.Measurements

  plug :validate_belongs_to_user when action in [:update, :delete]

  action_fallback(MomentsWeb.FallbackController)

  def index(conn, params) do
    user = Guardian.Plug.current_resource(conn)
    {:ok, fields} = Jason.decode(params.fields)

    measurements =
      Statistics.list_measurements(
        %{context: params.context, fields: fields},
        user
      )

    render(conn, "index.json", measurements: measurements)
  end

  def create(conn, %{measurements: measurements_params}) do
    user = Guardian.Plug.current_resource(conn)

    with {:ok, %Measurements{} = measurement} <-
           Statistics.create_measurement(measurements_params, user) do
      conn
      |> put_status(:created)
      |> put_resp_header("location", Routes.measurements_path(conn, :show, measurement))
      |> render("show.json", measurements: measurement)
    end
  end

  def update(conn, %{measurements: measurements_params}) do
    measurement = conn.assigns[:measurement]

    with {:ok, %Measurements{} = measurement} <-
           Statistics.update_measurement(measurement, measurements_params) do
      render(conn, "show.json", measurements: measurement)
    end
  end

  def delete(conn, _params) do
    measurement = conn.assigns[:measurement]

    with {:ok, %Measurements{}} <- Statistics.delete_measurement(measurement) do
      send_resp(conn, :no_content, "")
    end
  end

  defp validate_belongs_to_user(conn, _) do
    %{id: id} = conn.params
    user = Guardian.Plug.current_resource(conn)
    measurement = Statistics.get_measurement!(id)

    if measurement.user_id !== user.id do
      {:error, :unauthorized}
    else
      assign(conn, :measurement, measurement)
    end
  end
end
