defmodule MomentsWeb.Router do
  use MomentsWeb, :router

  alias MomentsWeb.Guardian

  pipeline :api do
    plug :accepts, ["json"]
    plug Plug.RequestId, http_header: "m-request-id"
    plug Plug.Logger, log: :debug
  end

  pipeline :jwt_authenticated do
    plug Guardian.AuthPipeline
  end

  scope "/api/v1", MomentsWeb do
    pipe_through :api

    post "/sign_up", UserController, :create
    post "/sign_in", UserController, :sign_in
  end

  scope "/api/v1", MomentsWeb do
    pipe_through [:api, :jwt_authenticated]

    get "/me", UserController, :show

    resources "/measurements", MeasurementsController
    get "/contexts", ContextsController, :index
  end
end
