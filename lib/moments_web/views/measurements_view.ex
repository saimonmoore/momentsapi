defmodule MomentsWeb.MeasurementsView do
  use MomentsWeb, :view
  alias MomentsWeb.MeasurementsView

  def render("index.json", %{measurements: measurements}) do
    %{data: render_many(measurements, MeasurementsView, "measurements.json")}
  end

  def render("show.json", %{measurements: measurements}) do
    %{data: render_one(measurements, MeasurementsView, "measurements.json")}
  end

  def render("measurements.json", %{measurements: measurements}) do
    %{
      id: measurements.id,
      field: measurements.field,
      value: measurements.value,
      description: measurements.description
    }
  end
end
