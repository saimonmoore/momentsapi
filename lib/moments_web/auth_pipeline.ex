defmodule MomentsWeb.Guardian.AuthPipeline do
  @moduledoc """
   Auth pipline
  """

  use Guardian.Plug.Pipeline,
    otp_app: :moments,
    module: Moments.Guardian,
    error_handler: Moments.AuthErrorHandler

  plug Guardian.Plug.VerifyHeader, realm: "Bearer"
  plug Guardian.Plug.EnsureAuthenticated
  plug Guardian.Plug.LoadResource
end
