defmodule Moments.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  def start(_type, _args) do
    configure_env(Mix.env())

    # List all child processes to be supervised
    children = [
      # Start the Ecto repository
      Moments.Repo,
      # Start the endpoint when the application starts
      MomentsWeb.Endpoint
      # Starts a worker by calling: Moments.Worker.start_link(arg)
      # {Moments.Worker, arg},
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: Moments.Supervisor]
    Supervisor.start_link(children, opts)
  end

  # Tell Phoenix to update the endpoint configuration
  # whenever the application is updated.
  def config_change(changed, _new, removed) do
    MomentsWeb.Endpoint.config_change(changed, removed)
    :ok
  end

  defp configure_env(_env) do
  end

  defp configure_env(:prod) do
    Application.put_env(:guardian, :secret_key, System.get_env("GUARDIAN_SECRET"))
  end
end
