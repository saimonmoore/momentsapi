defmodule Moments.Statistics do
  @moduledoc """
  The Statistics context.
  """

  import Ecto.Query, warn: false
  alias Moments.Repo

  alias Moments.Accounts.User
  alias Moments.Statistics.Measurements

  @doc """
  Queries measurements per user, context and field.

  ## Examples

      iex> list_measurements({context: :personal, fields: [:kpi1, :kpi2]}, user)
      [%Measurements{}, ...]

  """
  def list_measurements(%{context: context, fields: fields}, %User{} = user) do
    query =
      from m in Measurements,
        where: m.user_id == ^user.id and m.context == ^context and m.field in ^fields

    Repo.all(query)
  end

  @doc """
  Gets a single measurements.

  Raises `Ecto.NoResultsError` if the Measurements does not exist.

  ## Examples

      iex> get_measurement!(123, user)
      %Measurements{}

      iex> get_measurement!(456, user)
      ** (Ecto.NoResultsError)

  """
  def get_measurement!(id) do
    Repo.get!(Measurements, id)
  end

  @doc """
  Creates a measurements.

  ## Examples

      iex> create_measurement(%{field: value}, user)
      {:ok, %Measurements{}}

      iex> create_measurement(%{field: bad_value}, user)
      {:error, %Ecto.Changeset{}}

  """
  def create_measurement(attrs \\ %{}, %User{} = user) do
    now = DateTime.utc_now()

    %Measurements{}
    |> Measurements.changeset(
      Map.merge(attrs, %{
        user_id: user.id,
        timestamp: now
      })
    )
    |> Repo.insert()
  end

  @doc """
  Updates a measurements.

  ## Examples

      iex> update_measurement(measurement, %{field: new_value})
      {:ok, %Measurements{}}

      iex> update_measurement(measurement, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_measurement(%Measurements{} = measurement, attrs) do
    measurement
    |> Measurements.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a Measurements.

  ## Examples

      iex> delete_measurements(measurements)
      {:ok, %Measurements{}}

      iex> delete_measurements(measurements)
      {:error, %Ecto.Changeset{}}

  """
  def delete_measurement(%Measurements{} = measurement) do
    Repo.delete(measurement)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking measurements changes.

  ## Examples

      iex> change_measurement(measurements)
      %Ecto.Changeset{source: %Measurements{}}

  """
  def change_measurement(%Measurements{} = measurement) do
    Measurements.changeset(measurement, %{})
  end

  @doc """
  Returns a unique list of the users contexts

  ## Examples

      iex> list_contexts(user)
      [%Contexts{}, ...]

  """
  def list_contexts(%User{} = user) do
    query =
      from(
        m in Measurements,
        distinct: m.context,
        where: m.user_id == ^user.id,
        select: m.context
      )

    Repo.all(query)
  end
end
