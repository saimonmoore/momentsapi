import EctoEnum

defenum(MeasurementType, hilo: 0, counter: 1, timer: 2, gauge: 3)

defmodule Moments.Statistics.Measurements do
  @moduledoc """
    Schema for Measurements
  """

  use Ecto.Schema
  import Ecto.Changeset

  schema "measurements" do
    belongs_to :user, Moments.Accounts.User

    field :context, :string
    field :type, MeasurementType
    field :field, :string
    field :timestamp, :utc_datetime
    field :value, :integer
    field :description, :string

    timestamps()
  end

  @doc false
  def changeset(measurements, attrs) do
    measurements
    |> cast(attrs, [:user_id, :timestamp, :context, :type, :field, :value, :description])
    |> validate_required([:user_id, :timestamp, :context, :type, :field, :value])
  end
end
