defmodule MomentsWeb.MeasurementsControllerTest do
  use MomentsWeb.ConnCase

  alias Moments.Accounts
  alias Moments.Guardian
  alias Moments.Statistics
  alias Moments.Statistics.Measurements

  {:ok, now} = DateTime.now("Etc/UTC")

  @now DateTime.truncate(now, :second)
  @create_attrs %{
    context: "personal",
    type: :hilo,
    timestamp: @now,
    description: "some description",
    field: "field",
    value: 42
  }
  @update_attrs %{
    context: "personal",
    type: :hilo,
    timestamp: @now,
    description: "some updated description",
    field: "field",
    value: 43
  }
  @invalid_attrs %{description: nil, field: nil, value: nil}

  @create_user_attrs %{
    email: "some_email@foo.com",
    password: "some password",
    password_confirmation: "some password"
  }

  def fixture(:user) do
    {:ok, user} = Accounts.create_user(@create_user_attrs)
    user
  end

  def fixture(:measurements, user) do
    {:ok, measurements} = Statistics.create_measurement(@create_attrs, user)

    measurements
  end

  setup %{conn: conn} do
    user = fixture(:user)
    {:ok, jwt, _claims} = Guardian.encode_and_sign(user)

    conn =
      conn
      |> put_req_header("accept", "application/json")
      |> put_req_header("authorization", "Bearer #{jwt}")

    {:ok, conn: conn, user: user}
  end

  describe "index" do
    test "lists all measurements", %{conn: conn} do
      conn =
        get(
          conn,
          Routes.measurements_path(conn, :index, %{
            "context" => "personal",
            "fields" => "[\"field\"]"
          })
        )

      assert json_response(conn, 200)["data"] == []
    end
  end

  describe "create measurements" do
    test "renders measurements when data is valid", %{conn: conn} do
      conn = post(conn, Routes.measurements_path(conn, :create), measurements: @create_attrs)

      assert %{
               "id" => id,
               "description" => "some description",
               "field" => "field",
               "value" => 42
             } = json_response(conn, 201)["data"]
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = post(conn, Routes.measurements_path(conn, :create), measurements: @invalid_attrs)
      assert json_response(conn, 422)["errors"] != %{}
    end
  end

  describe "update measurements" do
    setup [:create_measurements]

    test "renders measurements when data is valid", %{
      conn: conn,
      measurements: %Measurements{id: id} = measurements
    } do
      conn =
        put(conn, Routes.measurements_path(conn, :update, measurements),
          measurements: @update_attrs
        )

      assert %{
               "id" => ^id,
               "description" => "some updated description",
               "field" => "field",
               "value" => 43
             } = json_response(conn, 200)["data"]
    end

    test "renders errors when data is invalid", %{conn: conn, measurements: measurements} do
      conn =
        put(conn, Routes.measurements_path(conn, :update, measurements),
          measurements: @invalid_attrs
        )

      assert json_response(conn, 422)["errors"] != %{}
    end
  end

  describe "delete measurements" do
    setup [:create_measurements]

    test "deletes chosen measurements", %{conn: conn, measurements: measurements} do
      conn = delete(conn, Routes.measurements_path(conn, :delete, measurements))
      assert response(conn, 204)
    end
  end

  defp create_measurements(%{user: user}) do
    measurements = fixture(:measurements, user)
    {:ok, measurements: measurements}
  end
end
