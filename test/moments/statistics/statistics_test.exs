defmodule Moments.StatisticsTest do
  use Moments.DataCase

  alias Moments.Statistics

  describe "measurements" do
    alias Moments.Statistics.Measurements
    alias Moments.Accounts
    alias Moments.Accounts.User

    now = DateTime.utc_now()

    @now DateTime.truncate(now, :second)
    @user_attrs %{
      email: "some_email@foo.com",
      password: "some password",
      password_confirmation: "some password"
    }
    @valid_attrs %{
      context: "personal",
      type: :hilo,
      description: "some description",
      field: "field",
      value: 42
    }
    @update_attrs %{
      context: "personal",
      type: :hilo,
      description: "some updated description",
      field: "field",
      value: 43
    }
    @invalid_attrs %{description: nil, field: nil, value: nil}

    def user_fixture(attrs \\ %{}) do
      {:ok, user} =
        attrs
        |> Enum.into(@user_attrs)
        |> Accounts.create_user()

      %User{user | password: nil, password_confirmation: nil}
    end

    def measurements_fixture(user, attrs \\ %{}) do
      {:ok, measurements} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Statistics.create_measurement(user)

      measurements
    end

    setup do
      user = user_fixture()
      {:ok, user: user}
    end

    test "list_measurements/2 returns measurements for user, context and fields", %{user: user} do
      measurements = measurements_fixture(user)

      assert Statistics.list_measurements(%{context: "personal", fields: ["field"]}, user) == [
               measurements
             ]
    end

    test "get_measurements!/1 returns the measurements with given id", %{user: user} do
      measurements = measurements_fixture(user)
      assert Statistics.get_measurement!(measurements.id) == measurements
    end

    test "create_measurements/1 with valid data creates a measurements", %{user: user} do
      assert {:ok, %Measurements{} = measurements} =
               Statistics.create_measurement(@valid_attrs, user)

      assert measurements.user_id == user.id
      assert DateTime.compare(DateTime.truncate(measurements.timestamp, :second), @now)
      assert measurements.description == "some description"
      assert measurements.field == "field"
      assert measurements.value == 42
    end

    test "create_measurements/1 with invalid data returns error changeset", %{user: user} do
      assert {:error, %Ecto.Changeset{}} = Statistics.create_measurement(@invalid_attrs, user)
    end

    test "update_measurements/2 with valid data updates the measurements", %{user: user} do
      measurements = measurements_fixture(user)

      assert {:ok, %Measurements{} = measurements} =
               Statistics.update_measurement(measurements, @update_attrs)

      assert measurements.user_id == user.id
      assert DateTime.compare(DateTime.truncate(measurements.timestamp, :second), @now)
      assert measurements.description == "some updated description"
      assert measurements.field == "field"
      assert measurements.value == 43
    end

    test "update_measurements/2 with invalid data returns error changeset", %{user: user} do
      measurements = measurements_fixture(user)

      assert {:error, %Ecto.Changeset{}} =
               Statistics.update_measurement(measurements, @invalid_attrs)

      assert measurements == Statistics.get_measurement!(measurements.id)
    end

    test "delete_measurements/1 deletes the measurements", %{user: user} do
      measurements = measurements_fixture(user)
      assert {:ok, %Measurements{}} = Statistics.delete_measurement(measurements)

      assert_raise Ecto.NoResultsError, fn ->
        Statistics.get_measurement!(measurements.id)
      end
    end

    test "change_measurements/1 returns a measurements changeset", %{user: user} do
      measurements = measurements_fixture(user)
      assert %Ecto.Changeset{} = Statistics.change_measurement(measurements)
    end
  end
end
